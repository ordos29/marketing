<?php
/**
 * User: Anton Kuzmenkov; kuzmenkov.anton@bitmaster.ru
 * Date: 15.04.16
 */

require_once __DIR__ . '/vendor/autoload.php';

$app = new Silex\Application();

require __DIR__ . '/app/app.php';

$app->run();

return $app['db.options'];
