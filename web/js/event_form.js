/**
 * Created by kuzmenkov on 08.04.16.
 */

window.onload = function () {
    jQuery(function ($) {
        $('#phone').mask('+7 (999) 9999999');
    });
};

function sendForm() {
    var user_name = $('input[name=user_name]').val();
    var user_id = $('input[name=user_id]').val();
    var event_name = $('input[name=event_name]').val();
    var user_email = $('input[name=user_email]').val();
    var user_phone = $('input[name=user_phone]').val();
    var incoming_params = $('input[name=incoming_params]').val();

    if (notEmpty([user_name, user_id, user_email,user_phone])) {
        $.ajax({
            method: 'POST',
            url: 'events_form_receiver',
            data: {
                user_name: user_name,
                user_id: user_id,
                event_name: user_name,
                user_email: user_email,
                user_phone: user_phone,
                incoming_params: incoming_params
            }
        }).done(function (answer) {
            $('#answer_title').html(answer.title);
            $('#answer_body').html(answer.body);
            $('#modalWindow').modal('toggle');
            $('#submit_btn').prop('disabled', true);
        });
    } else {
        alert('Не все поля заполнены.');
    }
}

function notEmpty(fields) {
    return fields.every(function (field) {
        return !/^\s*$/.test(field)
    })
}
