<?php
/**
 * User: Anton Kuzmenkov; kuzmenkov.anton@bitmaster.ru
 * Date: 14.04.16
 */

namespace Lib;


class MailChecker
{
    const CORRECT_EMAIL_PATTERN = '/^([a-z0-9_\.\-]{1,40})@([a-z0-9\.\-]{1,40})\.([a-z]{2,4})$/is';
    const NOT_EMAIL_ADDRESS = '/[^\@a-z0-9\-\_\.]/i';

    /**
     * @param $stringWithRawEmails string Массив корректных и не корректных адресов почты
     * @return array Массив массивов корректных и не корректных адресов почты
     */
    public static function separateCorrectAndBadEmails($stringWithRawEmails)
    {
        $rawEmails = static::splitAndCleanEmails($stringWithRawEmails);
        $correctEmails = [];
        $badEmails = [];

        foreach ((array)$rawEmails as $record) {
            if (static::isCorrectEmailAddress($record)) {
                $correctEmails[] = strtolower($record);
            } else {
                $badEmails[] = $record;
            }
        }

        return ['correct' => $correctEmails, 'bad' => $badEmails];
    }

    /**
     * @param $rawEmails string
     * @return array Массив адресов электронной почты
     */
    protected static function splitAndCleanEmails($rawEmails)
    {
        // Нектороые клиенты указывают кирилическую "с" вместо латинской!
        $rawEmails = str_replace(['С', 'с'], 'c', $rawEmails);
        $notEmailAddress = static::NOT_EMAIL_ADDRESS;
        $emails = preg_split($notEmailAddress, $rawEmails);
        // Маленькая кириллическая 'с' артефакт текущего алгоритма
        $emails = array_filter($emails, function ($email) {
            return !empty($email) && $email != 'c';
        });
        $cleanEmails = array_values(array_map('trim', $emails));

        return $cleanEmails;
    }

    /**
     * @param $emailAddress string Строка с проверяемым адресом почты
     * @return boolean
     */
    protected static function isCorrectEmailAddress($emailAddress)
    {
        return (bool)preg_match(static::CORRECT_EMAIL_PATTERN, $emailAddress);
    }

}
