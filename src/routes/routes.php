<?php
/**
 * User: Anton Kuzmenkov; kuzmenkov.anton@bitmaster.ru
 * Date: 13.04.16
 */

// Events

$app->get('/events', 'controllers\\EventsController::events');

$app->post('/events_form_receiver', 'controllers\\EventsController::eventsFormReceiver')->bind('events_form_receiver');

// Admin

$app->get('/', 'controllers\\MailAdminController::login');

$app->get('/login', 'controllers\\MailAdminController::login');

$app->get('/admin/logout', 'controllers\\MailAdminController::login');

$app->get('/admin', 'controllers\\MailAdminController::admin')->bind('admin');

$app->get('/manual_mail', 'controllers\\MailAdminController::manualMail')->bind('manual_mail');

$app->get('/load_emails_from_file', 'controllers\\MailAdminController::loadMailFromFile')->bind('load_emails_from_file');

$app->get('/unsubscribe', 'controllers\\MailController::unsubscribe')->bind('unsubscribe');

$app->post('/send_mail', 'controllers\\MailAdminController::sendMail')->bind('send_mail');

$app->post('/check_emails_from_file', 'controllers\\MailAdminController::checkEmailsFromFile')->bind('check_emails_from_file');
