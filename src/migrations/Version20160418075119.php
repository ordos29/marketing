<?php

namespace MarketingMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160418075119 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->createTable('unsubscribed');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('mail', 'string');
        $table->addColumn('subject', 'text');
        $table->addColumn('created_at', 'datetime', ['default' => 'now()']);
        $table->setPrimaryKey(['id']);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('unsubscribed');
    }
}
