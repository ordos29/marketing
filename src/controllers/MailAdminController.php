<?php
/**
 * User: Anton Kuzmenkov; kuzmenkov.anton@bitmaster.ru
 * Date: 13.04.16
 */

namespace Controllers;

use Silex\Application;
use Lib\MailChecker;

class MailAdminController
{
    const UPLOAD_DIRECTORY = '/../../web/upload/';

    public function login(Application $app)
    {
        return $app['twig']->render('login.twig', [
            'error'         => $app['security.last_error']($app['request']),
            'last_username' => $app['session']->get('_security.last_username'),
        ]);
    }

    public function startPage(Application $app)
    {
        return $app->redirect('/');
    }

    public function admin(Application $app)
    {
        return $app['twig']->render('admin.twig');
    }

    public function manualMail(Application $app)
    {
        return $app['twig']->render('manual_mail.twig', [
            'form' => [
                'from_manual'    => '',
                'subject'        => '',
                'message'        => '',
                'bad_emails'     => '',
                'correct_emails' => ''
            ]
        ]);
    }

    public function loadMailFromFile(Application $app)
    {
        return $app['twig']->render('load_emails_from_file.twig');
    }

    public function checkEmailsFromFile(Application $app)
    {
        $file = $app['request']->files->get('emails_file');
        $path = __DIR__ . static::UPLOAD_DIRECTORY;
        $filename = $file->getClientOriginalName();
        $file->move($path, $filename);

        $mailAddresses = MailChecker::separateCorrectAndBadEmails(file_get_contents($path . $filename));

        $badEmails = implode("\n", $mailAddresses['bad']);
        $correctEmails = implode("\n", $mailAddresses['correct']);

        return $app['twig']->render('manual_mail.twig', [
            'form' => [
                'from_manual'    => '',
                'subject'        => '',
                'message'        => '',
                'bad_emails'     => $badEmails,
                'correct_emails' => $correctEmails
            ]
        ]);
    }

    public function sendMail(Application $app)
    {
        $rawEmails = $app['request']->request->get('emails');
        $fromPredefined = $app['request']->request->get('from_manual');
        $fromManualOverwrite = $app['request']->request->get('from_manual');
        $subject = $app['request']->request->get('subject');
        $messageBody = $app['request']->request->get('message');
        $from = $fromManualOverwrite ?: $fromPredefined;
        $charset = $app['request']->request->get('charset');

        $mailAddresses = MailChecker::separateCorrectAndBadEmails($rawEmails);

        $badEmails = implode("\n", $mailAddresses['bad']);
        $correctEmails = implode("\n", $mailAddresses['correct']);

        if (!empty($badEmails)) {
            return $app['twig']->render('manual_mail.twig', [
                'form' => [
                    'from_manual'    => $from,
                    'subject'        => $subject,
                    'message'        => $messageBody,
                    'bad_emails'     => $badEmails,
                    'correct_emails' => $correctEmails
                ]
            ]);
        } else {
            $sql = "SELECT mail FROM unsubscribed";
            $unsubscribed = $app['db']->fetchAll($sql);

            $unsubscribed = array_map(function ($row) {
                return $row['mail'];
            }, $unsubscribed);

            $recipients = array_diff($mailAddresses['correct'], $unsubscribed);

            if ($charset == 'windows-1251') {
                $messageBody = iconv('UTF-8', 'WINDOWS-1251', $messageBody);
                $subject = iconv('UTF-8', 'WINDOWS-1251', $subject);
            }

            foreach ($recipients as $recipient) {
                $unsubscribeParams = [
                    'mail'    => $recipient,
                    'subject' => $subject
                ];
                $unsubscribeUrl = $app['url_generator']->generate('unsubscribe', $unsubscribeParams);

                $messageBody = str_replace('%unsubscribe_url%', $unsubscribeUrl, $messageBody);

                $message = \Swift_Message::newInstance()
                    ->setSubject($subject)
                    ->setFrom($from)
                    ->setTo($recipient)
                    ->setContentType('text/html')
                    ->setBody($messageBody);

                $app['mailer']->send($message);
            }
        }
    }
}
