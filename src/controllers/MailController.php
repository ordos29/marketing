<?php
/**
 * User: Anton Kuzmenkov; kuzmenkov.anton@bitmaster.ru
 * Date: 19.04.16
 */

namespace controllers;

use Silex\Application;

class MailController
{
    public function unsubscribe(Application $app)
    {
        $mail = $app['request']->query->get('mail');
        $subject = $app['request']->query->get('subject');

        $requiredParamsPresent = $mail && $subject;

        if ($requiredParamsPresent === false) {
            $app->abort(404, 'Не хватает параметров!');
        }

        $sql = "INSERT INTO unsubscribed(mail, subject) VALUES(:mail, :subject)";
        $app['db']->executeUpdate($sql, [
            'mail'    => strval($mail),
            'subject' => strval($subject)
        ]);

        return 'Вы отписаны от рассылки';
    }
}
