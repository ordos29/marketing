<?php
/**
 * User: Anton Kuzmenkov; kuzmenkov.anton@bitmaster.ru
 * Date: 13.04.16
 */

namespace Controllers;

use Silex\Application;
use Swift_Message;

class EventsController
{
    public function events(Application $app)
    {
        return $app['twig']->render('events_form.twig');
    }

    public function eventsFormReceiver(Application $app)
    {
        $userId = $app['request']->request->get('user_id');
        $userName = $app['request']->request->get('user_name');
        $userPhone = $app['request']->request->get('user_phone');
        $userEmail = $app['request']->request->get('user_email');
        $eventName = $app['request']->request->get('event_name');

        $messageBody = sprintf("%s (ИД%s) зарегистрировался, e-mail: %s",
            $userName,
            $userId,
            $userEmail
        );

        if ($userPhone) {
            $messageBody .= ", контактный телефон: $userPhone";
        }

        if (empty($userEmail) || empty($eventName)) {
            $messageBody .= PHP_EOL . PHP_EOL . 'Что-то пошло не так, это письмо нужно переслать веб-разработчикам' .
                PHP_EOL . $app['request']->request->get('incoming_params', '');
        }

        $message = Swift_Message::newInstance(sprintf('Регистрация на "%s"', $eventName))
            ->setFrom($app['config']['events']['address_from'])
            ->setTo($app['config']['events']['address_to'])
            ->setBody($messageBody);

        $app['mailer']->send($message);

        $app['clients_logger']->addInfo($messageBody);

        $modalHeader = $app['config']['events']['modal_header'];
        $modalBody = $app['config']['events']['modal_body'];

        return $app->json(['title' => $modalHeader, 'body' => $modalBody]);
    }
}
