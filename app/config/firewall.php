<?php
/**
 * User: Anton Kuzmenkov; kuzmenkov.anton@bitmaster.ru
 * Date: 13.04.16
 */

// Для генерации паролей пользователей
$user = new Symfony\Component\Security\Core\User\User('new', 'foo');
$encoder = $app['security.encoder_factory']->getEncoder($user);

$usersConfig = $app['config']['users'];

$users = [];
foreach ($usersConfig as $name => $params) {
    $users[$name] = [$params['role'], $encoder->encodePassword($params['password'], $user->getSalt())];
}

$firewall = [
    'admin' => [
        'pattern' => '^/admin',
        'form'    => [
            'login_path'          => '/login',
            'check_path'          => '/admin/login_check',
            'default_target_path' => '/admin'],
        'logout'  => [
            'logout_path'        => '/admin/logout',
            'invalidate_session' => true
        ],
        'users'   => $users,
    ],
];

$accessRules = [
    ['^/admin', 'ROLE_ADMIN'],
];
