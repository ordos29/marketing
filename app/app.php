<?php

use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;

/**
 * @var $app Silex\Application
 */

$app->error(function (\Exception $e) {
    return new Response($e->getMessage());
});

$app->register(new DerAlex\Silex\YamlConfigServiceProvider(__DIR__ . '/config/config.yml'));

//$app['debug'] = true;

$app['locale'] = $app['config']['locale'];

$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

$app->register(new Silex\Provider\TwigServiceProvider(), ['twig.path' => __DIR__ . '/../src/template']);

$app->before(function () use ($app) {
    $app['twig']->addGlobal('admin_layout', null);
    $app['twig']->addGlobal('admin_layout', $app['twig']->loadTemplate('admin_layout.twig'));
});

$app->register(new Silex\Provider\SwiftmailerServiceProvider());
$app['mailer'] = \Swift_MailTransport::newInstance();

$app->register(new Silex\Provider\SessionServiceProvider());

$app['clients_logger'] = function () {
    $log = new Logger('clients');

    return $log->pushHandler(new RotatingFileHandler(__DIR__ . '/../log/event_registrations.log', 14, Logger::INFO));
};

$app->register(new Silex\Provider\SecurityServiceProvider(), []);

require_once __DIR__ . '/config/firewall.php';

$app['security.firewalls'] = $firewall;

$app['security.access_rules'] = $accessRules;

$app->register(new Silex\Provider\TranslationServiceProvider(), ['locale_fallbacks' => ['en']]);

$app['translator.domains'] = $app['config']['translate'];

$app->register(new Silex\Provider\DoctrineServiceProvider(), [
    'db.options' => [
        'driver'   => $app['config']['database']['driver'],
        'dbname'   => $app['config']['database']['dbname'],
        'host'     => $app['config']['database']['host'],
        'user'     => $app['config']['database']['user'],
        'password' => $app['config']['database']['password'],
    ],
]);

return $app;
